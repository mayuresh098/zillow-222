#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: mayuresh
#  
#  
url = "http://files.zillowstatic.com/research/public/County/County_Zhvi_AllHomes.csv"


# first approach manually put all the files in a dic format
# not dynamic enough
master_all_hyper_links={
"ZHVI Summary Current Month":{"state":"http://files.zillowstatic.com/research/public/State/State_Zhvi_Summary_AllHomes.csv","county":"http://files.zillowstatic.com/research/public/County/County_Zhvi_Summary_AllHomes.csv"},
"ZHVI All Homes (SFR CondoCo op) Time Series":{"state":"http://files.zillowstatic.com/research/public/State/State_Zhvi_AllHomes.csv", "county":"http://files.zillowstatic.com/research/public/County/County_Zhvi_AllHomes.csv"},
"ZHVI All Homes- Bottom Tier Time Series":{"state":"http://files.zillowstatic.com/research/public/State/State_Zhvi_BottomTier.csv","county":"http://files.zillowstatic.com/research/public/County/County_Zhvi_BottomTier.csv"}, 
"ZHVI All Homes- Top Tier Time Series":{"state":"http://files.zillowstatic.com/research/public/State/State_Zhvi_TopTier.csv","county":"http://files.zillowstatic.com/research/public/County/County_Zhvi_TopTier.csv"}, 
}
'''
######################Not to self : you can do better, on hold cant do this forever
"ZHVI Condo-Co-op Time Series":{"state":,"county":}, 
"ZHVI Single-Family Homes Time Series":{"state":,"county":}, 
"ZHVI 1-Bedroom Time Series":{"state":,"county":}, 
"ZHVI 2-Bedroom Time Series":{"state":,"county":}, 
"ZHVI 3-Bedroom Time Series":{"state":,"county":}, 
"ZHVI 4-Bedroom Time Series":{"state":,"county":}, 
"ZHVI 5+ Bedroom Time Series":{"state":,"county":}, 
"Median Home Value Per Sq Ft":{"state":,"county":}, 
"Zillow Home Value Forecast":{"state":,"county":},
"Quarterly Historic Metro ZHVI":{"state":,"county":},
"Increasing Values":{"state":,"county":}, 
"Decreasing Values":{"state":,"county":} 
with closing(requests.get(url, stream=True)) as r:
    reader = csv.reader(r.iter_lines(), delimiter=',', quotechar='"')
    for row in reader:
        print row  
'''

#part-2
#real fun starts here

'''
master_url="https://www.zillow.com/research/data/"


#!/usr/bin/env python

from selenium import webdriver

browser = webdriver.Firefox(executable_path="/home/taz1/Documents/data/zillow-222/master/geckodriver")
browser.get(master_url)



html_source = browser.page_source


file1 = open("html.txt","w") 

file1.write(html_source) 
file1.close() #to change file acc
'''

'''

soup = BeautifulSoup(html_doc, 'html.parser')

print(soup.prettify())
'''
'''

#better apparoah is to get the div of drop down 1 and drop2 and then get the download button link
for city in soup.find_all('span', {'class' : 'select-wrapper'}):
	
    print(city)
    print ("**********")
'''

import time
start_selenium_match = time.time()
import requests
import csv
import re
import os
import time
from selenium import webdriver
from contextlib import closing
from bs4 import BeautifulSoup

from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.options import Options

options = Options()#for headless firefox
options.headless = True

############global decalarations
cur_dir = os.getcwd()
master_url="https://www.zillow.com/research/data/"

#########################CONTROL PANEL###################################
timer_wait=5

#before begining we will save a temp file with all the source code
browser = webdriver.Firefox(options=options,executable_path=cur_dir+"/geckodriver")# not sure how this dependencies will be cleared in lambda
browser.get(master_url)

html_source = browser.page_source
file1 = open(cur_dir+"/tmp/html.txt","w") 
file1.write(html_source) 
file1.close() #to change file acc
soup = BeautifulSoup(open(cur_dir+"/tmp/html.txt"), "html.parser")



#######script_name and other file identifications will come here
script_name="zillow_downloader_s3_master"
projectname="zillow"
##################functions 

def down_file(file_url,local_path):
	'''
	used to save the file locally from some extenal url
	if invalid url return -1
		1>if url has less than two "/"
	else download the file at mentione path
	'''
	extension= (file_url.split("/"))
	if len(extension)<2:
		return -1
	r = requests.get(file_url) # create HTTP response object 
	local_filename=extension[len(extension)-1]
	#print (local_filename)
	#/home/taz1/Documents/data/zillow-222/master/tmp
	#print (cur_dir+"/tmp/"+local_filename)
	
	
	# send a HTTP request to the server and save 
	# the HTTP response in a response object called r 
	with open(cur_dir+"/tmp/"+local_filename,'wb') as f:
		f.write(r.content) 





download_only=["state","county"]
######################################################################################3
# now we try to load the files via combined zip 
# and then later corelate them with the selenium script of each file name
#! the idea is to 
master_zip_list={
"state":"http://files.zillowstatic.com/research/public/State.zip",
"metro":"http://files.zillowstatic.com/research/public/Metro.zip",
"county":"http://files.zillowstatic.com/research/public/County.zip",
"city":"http://files.zillowstatic.com/research/public/City.zip",
"zip_codes":"http://files.zillowstatic.com/research/public/Zip.zip",
"neighborhood":"http://files.zillowstatic.com/research/public/Neighborhood.zip"
}


#down_file("http://files.zillowstatic.com/research/public/Neighborhood.zip","/tmp")







master_dic={}

for form_ele in soup.find_all('form'):
	#better apparoah is to get the div of drop down 1 and drop2 and then get the download button link
       
    inner_select= (form_ele.find_all("select"))
    # the element has not select and drop down box
    if(len(inner_select)>1):
      print ("**********")
      # gettign the id of the anchor tag button (its the only anchor within this class)
      anchor_tag=(form_ele.find('a'))
      anchor_tag_id=anchor_tag.get_attribute_list('id')
      #print (anchor_tag_id[0])
      anchor_pointer=soup.find(id=str(anchor_tag_id[0]))# will be used to get the file url later
      #print (anchor_pointer.get("href"))
      #print (soup.find(id=str(anchor_tag_id[0])))
      #print ("intital link:-",soup.find_element_by_id("median-home-value-zillow-home-value-index-zhvi-download-link"))
      #print (len(anchor_tag))
      #print (anchor_tag,type(anchor_tag))
      #print (anchor_tag.getAttribute("href"))
      
      #print (dir(anchor_tag))
      #print ()
      #print (form_ele)
      #continue
      #getting the dynamic drop downlist-lits1 count
      span_wrapper=(form_ele.find('select', {'class' : 'data-dropdown-1'}))
      get_options_in_span_wrapper=span_wrapper.find_all("option")
      #print ("length of list:-",(len(get_options_in_span_wrapper)))
      
      # counting the elements in dropdowns which will be refered via index
      count_drop1_element=len(get_options_in_span_wrapper)
      
      #getting the dynamic drop downlist-lits1 count
      span_wrapper=(form_ele.find('select', {'class' : 'data-dropdown-2'}))
      get_options_in_span_wrapper2=span_wrapper.find_all("option")
      #print ("length of drop down list2:-",(len(get_options_in_span_wrapper2)))
      count_drop2_element=len(get_options_in_span_wrapper2)
      
      span_wrapper1=(form_ele.find('select', {'class' : 'data-dropdown-1'}))
      id_of_drop1_select= (span_wrapper1.get_attribute_list('id').get_attribute_list('id') )
      
      span_wrapper2=(form_ele.find('select', {'class' : 'data-dropdown-2'}))
      id_of_drop2_select= (span_wrapper2.get_attribute_list('id') )
      time.sleep(timer_wait)
      
      #intializing the select pointer to dropdown 
      select_drop1= Select(browser.find_element_by_id(str(id_of_drop1_select[0])))
      select_drop2=Select(browser.find_element_by_id(str(id_of_drop2_select[0])))
      
      #######looping in counted drop downfields and slecting each with m*n ratio
      try:
        for wrp1 in range(0,count_drop1_element):# we only need first 4
          #print ("element is ",wrp1)
          select_drop1.select_by_index(wrp1)
          element_text_drop1_text=get_options_in_span_wrapper[wrp1].text
          element_text_drop1_text=element_text_drop1_text.lower()
          #print ("element drop1 is",element_text_drop1_text)
          time.sleep(timer_wait)
          
          
          #getting the dynamic drop downlist-lits1 count
          
          #getting the new second list
          html_source = browser.page_source
          soup_new = BeautifulSoup(html_source, "html.parser")
          
          span_wrapper=(soup_new.find('select', {'class' : 'data-dropdown-2'}))
          get_options_in_span_wrapper2=span_wrapper.find_all("option")
          #print ("length of drop down list2:-",(len(get_options_in_span_wrapper2)))
          #counting the second drop down element again in case it has changed
          count_drop2_element=len(get_options_in_span_wrapper2)
          
          for wrp2 in range(0,count_drop2_element):
            try:
              #print (dir(get_options_in_span_wrapper2[wrp2]))
              element_text=(get_options_in_span_wrapper2[wrp2].text)
              element_text=element_text.lower()
              #element_text=re.sub(r'[^a-zA-Z0-9 ]',r'',element_text)# removing the special charaters to make them easy to save and store
              #print ("selected second element is",element_text)
              '''
              # removing this will download all the files
              if element_text not in download_only:
                continue
              '''				
              select_drop2.select_by_index(wrp2)
              anchor_tag=(form_ele.find('a'))
              html_source = browser.page_source
              soup_new = BeautifulSoup(html_source, "html.parser")
              anchor_elem=soup_new.find(id=str(anchor_tag_id[0]))
              file_link= (anchor_elem.get('href'))# getting the anchor tag via the previously calculated unique id
              
              # data struct of dict of dict
              #element_text_drop1_text{element_text:file_link}
              
              #appending the drop1 Aand drop2 in dict plus the link
              if element_text_drop1_text in master_dic:
                #print("exits")# it would exist when we select multiple state
                #master_dic[element_text_drop1_text].append({element_text:file_link})
                master_dic[element_text_drop1_text].append({element_text:file_link}) 
              else:
                master_dic[element_text_drop1_text]=[{element_text:file_link}]
              #print (master_dic)
              #print(file_link)
              #down_file(file_link,"/tmp")
              
            except Exception as inner_e:
              print ("inner e exception as ",inner_e)


            time.sleep(timer_wait)
      except Exception as e:
        print (e)

end_selenium_match = time.time()
print(end_selenium_match - start_selenium_match)


print (master_dic)

'''
# not working lets not stick to it
with open(cur_dir+"/tmp/dict_output.txt",'wb') as f:
  f.write((master_dic.encode())) 

with open(cur_dir+"/tmp/dict_output.txt",'wb') as f:
  f.write(str(master_dic)) 
'''

#end_selenium_match = time.time()
#print(end_selenium_match - start_selenium_match)
